//! This module implements the `OpkgError` type

use std::convert::From;
use std::error::Error;
use std::fmt::Display;

/// Represents an error in the execution of the program 
#[derive(Debug)]
pub enum OpkgError {
  /// command error
  CommandErr {
    /// name of the command
    name: String,
    /// message to display
    message: String,
  },

  /// read error
  ReadErr {
    /// name of file being read
    name: String,

    /// message to display
    message: String,
  },

  /// write error
  WriteErr {
    /// name of the file being written to
    name: String,

    /// message to display
    message: String,
  },

  /// filesystem metadata error
  Metadata(String),

  /// miscellaneous error
  Misc,

  /// serialization/deserialization error
  Serde(String),
}

impl Display for OpkgError {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
    match self {
      Self::CommandErr  { name, message } => write!(f, "Command '{}' failed with reason: {}", name, message),
      Self::ReadErr     { name, message } => write!(f, "Reading from file ({}) failed! \n{}", name, message),
      Self::WriteErr    { name, message } => write!(f, "Writing to file ({}) failed!   \n{}", name, message),
      Self::Metadata    (s)               => write!(f, "File ({}) does not exist!", s),
      Self::Misc                          => write!(f, "Miscellaneous error."),
      Self::Serde       (s)               => write!(f, "Serialization/deserialzation error:\n{}", s)
    }
  }
}

impl From<std::io::Error> for OpkgError {
  fn from(err: std::io::Error) -> Self {
    OpkgError::Metadata(err.to_string())
  }
}

impl From<toml::de::Error> for OpkgError {
  fn from(err: toml::de::Error) -> Self {
    OpkgError::Serde(err.to_string())
  }
}

impl Error for OpkgError {
}
