//! The Orion package management and build tool
#![crate_name = "op"]
#![crate_type = "bin"]
#![warn(missing_docs)]
#![allow(dead_code)]
#![warn(clippy::all)]
#![allow(clippy::needless_return)]
#![warn(missing_debug_implementations)]

// Dependencies
#[macro_use]
extern crate lazy_static;
extern crate serde;
#[macro_use]
extern crate serde_derive;

pub mod config;
pub mod error;
pub mod version;

pub use self::error::OpkgError;
pub use self::version::Version;

/// Name of the program
pub const NAME: &str = "opkg";

/// Description of the program
pub const DESCRIPTION: &str = "Orion's build tool";

lazy_static! {
  /// Arguments used to start the program
  pub static ref ARGS: Vec<String> = std::env::args().collect();

  /// Version of the program
  pub static ref VERSION: Version = Version::new(0, 1, 0, "SNAPSHOT");
}

fn main() -> Result<(), OpkgError> {
  // Load configuration
  let args  = &self::ARGS;
  let _conf = config::load()?;
  // ---

  // Run arguments against available commands
  match args[0].as_str() {
    "greet" => println!("Hey!"),
    _       => println!("Not enough arguments were provided!"),
  }
  // ---

  return Ok(());
}
