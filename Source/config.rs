//! Configuration types and functions

use crate::{OpkgError, Version};
use std::io::Read;
use std::io::Write;
use std::fs::{
  self,
  File,
};

#[derive(Debug, Deserialize, Serialize)]
struct BuildConfig {
  target: String,
}

/// Represents the application configuration
#[derive(Debug, Deserialize, Serialize)]
pub struct Config {
  /// the target to build for
  build_config: BuildConfig,

  /// version of the application 
  version: Version,
}

/// Saves the configuration manifest
pub fn save() -> Result<(), OpkgError> {
  let conf = Config {
    build_config: BuildConfig {
      target: "host".to_string(),
    },
    
    version: Version::default(),
  };

  fs::create_dir(".orion")?;
  let mut fi = File::create(".orion/config")?;

  let s = toml::to_string_pretty(&conf).unwrap();
  fi.write_all(s.as_bytes()).unwrap();

  return Ok(());
}

/// Loads the configuration manifest
pub fn load() -> Result<Config, OpkgError> {
  if let Err(e) = fs::metadata(".orion/config") {
    eprintln!("Config file doesn't exist!\n{}", e);
    
    if let Err(e) = self::save() {
      eprintln!("Error saving the config!\n{}", e);

      return Err(e);
    }

    return Err(e.into());
  } else {
    let conf: Config;
    let mut s = String::new();
    let mut fi = File::open(".orion/config")?;
    fi.read_to_string(&mut s).unwrap();

    conf = toml::from_str(s.as_str())?;

    return Ok(conf);
  }
}
