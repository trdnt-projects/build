/*!
# Module opkg::version

Semantic versioning for the Opkg tool
*/

#[allow(unused_imports)]
use serde::{Deserialize, Serialize};

/// Semantic versioning for Opkg
#[derive(Debug, Deserialize, Serialize)]
pub struct Version {
  major: u32,
  minor: u32,
  revision: u32,
  status: String,
}

impl Version {
  /// Creates a new instance of the 'Version' struct using the supplied parameters
  pub fn new(major: u32, minor: u32, revision: u32, status: &'static str) -> Version {
    return Version {
      major,
      minor,
      revision,
      status: status.to_string(),
    };
  }

  /// Gets a string representing the version of the application
  pub fn get_string(&self) -> String {
    return format!("{}.{}.{}::{}", self.major, self.minor, self.revision, self.status);
  }

  /// Gets the 'major' version of the application
  pub fn major(&self) -> u32 {
    return self.major;
  }

  /// Gets the 'minor' version of the application
  pub fn minor(&self) -> u32 {
    return self.minor;
  }

  /// Gets the 'revision' of the application
  pub fn revision(&self) -> u32 {
    return self.revision;
  }

  /// Gets the version "status"
  pub fn status(&self) -> String {
    return self.status.clone();
  }
}

impl Default for Version {
  fn default() -> Version {
    return Version {
      major: 0,
      minor: 1,
      revision: 0,
      status: "SNAPSHOT".to_string(),
    };
  }
}
