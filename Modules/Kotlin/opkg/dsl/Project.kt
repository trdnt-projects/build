package opkg.dsl

data class Project(
        val name: String,
        val description: String,
        val version: String,
        val language: String,
        val authors: Array<String>,
        val categories: Array<String>,
        val publish: Boolean,
)
