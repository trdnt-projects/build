package opkg.dsl

data class Dependencies(
        val deps: Array<String>,
        val dev_deps: Array<String>,
        val build_deps: Array<String>,
)
