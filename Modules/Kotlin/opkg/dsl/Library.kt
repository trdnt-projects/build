package opkg.dsl

data class Library(
        val name: String,
        val sources: Array<String>,
        val pkg_type: String,
)
