package opkg.dsl

data class Workspace(
        val members: Array<String>,
        val excluded: Array<String>,
)