package opkg.dsl

data class Overrides(
        val build_system: String, // Build system name
        val language: String,
)
