package opkg.dsl

data class Application(
        val name: String,
        val sources: Array<String>,
)
