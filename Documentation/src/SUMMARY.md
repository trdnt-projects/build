# Summary

- [Welcome](./welcome.md)
- [Getting Started](./getting_started.md)
- [Installing](./installing/index.md)
  * [Windows](./installing/windows.md)
  * [Linux](./installing/linux/index.md):
    - [Debian / Ubuntu](.installing/linux/debian.md)
    - [Fedora](./installing/linux/fedora.md)
    - [Solus](./installing/linux/solus.md)
    - [Arch](https://javascript:void(0)) *(coming soon)*
  * [macOS](./installing/macos.md)
- [Contribution Guide](./contributions.md)
- [Reference](./reference/index.md)
- [About](./about.md)
