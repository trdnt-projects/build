# Welcome to the OPM docs!

- Version: 0.1.0
- Last Updated: 2020.04.14 @ 01:25 EST
- Recent Additions: [Contributions](./contribution_guide.md)

---

### Getting Started with OPM
- If you wish to jump right in and get started with OPM, visit the [Getting Started](./getting_started.md) page.

### Contributing to the OPM project
- If you wish to contribute the project, start [here](./contribution_guide.md).

### Learn about OPM
- If you wish to learn about the project, visit the [About](./about.md) page.

### A Reference
-  If you wish to see a reference, visit the [OPM Reference](./reference.md) page.

---

Thank you for your interest in the OPM project!
