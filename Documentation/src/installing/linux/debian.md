# Debian and Ubuntu

### Add the Repository
```bash
sudo echo "deb [trusted=yes] https://raw.githubusercontent.com/orionstudios/opkg/debian stretch main" > /etc/sources.list/orion.list
```

### Install
```bash
sudo apt update
sudo apt install orion-pkg
```
