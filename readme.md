# Opkg: Orion's package manager

Version: 0.1.0 | [Getting Started](https://projectorion.us/opkg/getting_started) | [Contributing](https://projectorion.us/opkg/contributions)

---

## Installing

No installable binaries are currently available.
`opkg` must be built from source.

- Clone this repo
- Run `cargo build --release`
- Run `cargo install --path .`

## Using Opkg

Using the Opkg tool is straightforward: all that's really needed is to install it to a location  
in your PATH and call it on the command line.  

Once Opkg is installed, you can call it from a command line and operate on your package from there.

To create a project, you can use `opkg new`. The generated '.orion/package' file will look like this (assuming no parameters are supplied):

```toml
[project]
name = "<PROJECT_DIRECTORY_NAME>"
description = "Describe your project"
categories = ["Categorize", "your", "project"]
authors = ["Author", "names", "and <email@addresses>"]
version = "0.1.0"
publish = false

[dependencies]
# List your dependencies here
```

To build a project, you can run ```opkg build``` and the project will build with the corresponding build system:

- [Ninja](https://ninja-build.org) (default)
- [GNU Make](https://www.gnu.org/software/make) (least preferred)
- [NMake](https://docs.microsoft.com/en-us/cpp/build/reference/nmake-reference?view=vs-2019) (for Visual C/C++)

More languages will be added over time.

*This is an open-source project.*

---

Thank you for your interest in Orion Studios projects!
